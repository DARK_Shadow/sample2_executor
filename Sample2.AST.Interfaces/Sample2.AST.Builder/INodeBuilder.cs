﻿using System.Runtime.InteropServices;

namespace Sample2.AST.Builder
{
    [Guid("1BC35F01-DD66-45A0-9396-76F592DD50D4")]
    [ComVisible(true), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface INodeBuilder
    {
        void StartStatements();
        void BuildStatements();
        void BuildAssignStatement();
        void BuildDisplayStatement();
        void BuildDisplayReadStatement();
        void BuildIfThenStatement();
        void BuildIfThenElseStatement();
        void BuildWhileStatement();

        void BuildBinaryExpression();
        void BuildExpressionValue();
        void BuildNegateValue();
        void BuildVariable(string name);
        void BuildNumberConst(double value);
        void BuildStringConst(string value);

        void BuildAddOp();
        void BuildSubOp();
        void BuildMulOp();
        void BuildDivOp();
        void BuildEqOp();
        void BuildNEqOp();
        void BuildLTOp();
        void BuildLTOEqOp();
        void BuildGTOp();
        void BuildGTOEqOp();
    }
}