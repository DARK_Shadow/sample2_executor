using System;
using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    [Terminal("NumberLiteral")]
    public class NumberConst : Const, INumberConst
    {
        private readonly double value;

        public NumberConst(string value)
        {
            this.value = double.Parse(value);
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_NumCnst(this);
        }

        public double Value
        {
            get { return value; }
        }
    }
}