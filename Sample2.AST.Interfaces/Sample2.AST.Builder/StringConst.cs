using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    [Terminal("StringLiteral")]
    public class StringConst : Const, IStringConst
    {
        private readonly string value;
        
        public StringConst(string value)
        {
            this.value = value;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_StrCnst(this);
        }

        public string Value
        {
            get { return value; }
        }
    }
}