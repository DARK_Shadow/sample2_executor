﻿using System.Runtime.InteropServices;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    [Guid("8CCB0AA7-6691-444F-9578-EF0DE0FE82A7")]
    [ComVisible(true), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IBuilder
    {
        INode Build(string source);
        void BuildNodes(string source, INodeBuilder builder);
    }
}
