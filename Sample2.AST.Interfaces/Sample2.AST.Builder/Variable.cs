using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    [Terminal("Id")]
    public class Variable : Value, IVariable
    {
        private readonly string name;

        public Variable(string name)
        {
            this.name = name;
        }

        public string Name
        {
            get { return name; }
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_Var(this);
        }
    }
}