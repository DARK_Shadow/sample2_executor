using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public abstract class Expression : Node,  IExpression { }
}