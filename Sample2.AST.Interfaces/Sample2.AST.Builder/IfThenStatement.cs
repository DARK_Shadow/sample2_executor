using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public class IfThenStatement : Statement,  IIfThenStatement
    {
        private readonly Expression condition;
        private readonly Statements thenStatements;
        
        [Rule(@"<Statement> ::= ~if <Expression> ~then <Statements> ~end")]
        public IfThenStatement(Expression condition, Statements thenStatements)
        {
            this.condition = condition;
            this.thenStatements = thenStatements;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_IfThStmt(this);
        }

        public IExpression Condition
        {
            get { return condition; }
        }

        public IStatements ThenBody
        {
            get { return thenStatements; }
        }
    }
}