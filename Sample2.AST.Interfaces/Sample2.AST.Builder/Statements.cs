﻿using System.Collections.Generic;
using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public class Statements: Sequence<Statement>, IStatements
    {
        class StatementsEnumerator : IStatementsEnumerator
        {
            private readonly IEnumerator<Statement> statements;

            public StatementsEnumerator(IEnumerator<Statement> statements)
            {
                this.statements = statements;
            }

            public IStatement Current
            {
                get { return statements.Current; }
            }

            public bool MoveNext()
            {
                return statements.MoveNext();
            }
        }

        [Rule("<Statements> ::= <Statement>")]
        public Statements(Statement item)
            : base(item)
        {
        }

        [Rule("<Statements> ::= <Statement> <Statements>")]
        public Statements(Statement item, Sequence<Statement> next)
            : base(item, next)
        {
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_Stmts(this);
        }

        public new IStatementsEnumerator GetEnumerator()
        {
            return new StatementsEnumerator(base.GetEnumerator());
        }
    }
}
