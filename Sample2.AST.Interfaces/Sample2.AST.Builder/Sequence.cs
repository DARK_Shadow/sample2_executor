using System.Collections;
using System.Collections.Generic;
using bsn.GoldParser.Semantic;

namespace Sample2.AST.Builder
{
    public class Sequence<T> : Node, IEnumerable<T> where T : Node
    {
        private readonly T item;
        private readonly Sequence<T> next;

        public Sequence(T item) : this(item, null) { }

        public Sequence(T item, Sequence<T> next)
        {
            this.item = item;
            this.next = next;
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (var sequence = this; sequence != null; sequence = sequence.next)
            {
                if (sequence.item != null)
                {
                    yield return sequence.item;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}