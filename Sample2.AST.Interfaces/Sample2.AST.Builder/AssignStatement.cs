using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public class AssignStatement : Statement, IAssignStatement
    {
        private readonly Variable variable;
        private readonly Expression expression;

        [Rule(@"<Statement> ::= ~assign Id ~'=' <Expression>")]
        public AssignStatement(Variable variable, Expression expression)
        {
            this.variable = variable;
            this.expression = expression;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_AsgnStmt(this);
        }

        public IVariable Variable
        {
            get { return variable; }
        }

        public IExpression Expression
        {
            get { return expression; }
        }
    }
}