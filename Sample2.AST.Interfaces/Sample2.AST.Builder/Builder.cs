﻿using System;
using System.IO;
using System.Linq;
using Sample2.AST.Interfaces;
using bsn.GoldParser.Grammar;
using bsn.GoldParser.Semantic;
using System.Runtime.InteropServices;
using bsn.GoldParser.Parser;
using Sample2.AST.Builder;

[assembly: RuleTrim(@"<Add Exp> ::= <Add Exp> '&' <Mult Exp>", "<Mult Exp>", SemanticTokenType = typeof(Node))]

namespace Sample2.AST.Builder
{
    [Guid("0348C4B2-1D55-426A-B54A-2FDEF582260F")]
    [ComVisible(true), ClassInterface(ClassInterfaceType.None)]
    public class Builder : IBuilder
    {

        private Node Parse(string source)
        {
            var grammar = CompiledGrammar.Load(typeof(Node), "Simple 2.egt");
            var actions = new SemanticTypeActions<Node>(grammar);

            actions.Initialize(true, true);

            var processor = new SemanticProcessor<Node>(new StringReader(source), actions);
            var parseMessage = processor.ParseAll();

            if (parseMessage == ParseMessage.Accept)
            {
                return processor.CurrentToken;
            }
            else
            {
                IToken token = processor.CurrentToken;
                throw new Exception(string.Format("Line:{0} Col:{1} Error:{2} Symbol:'{3}' Expected:{4}",
                    token.Position.Line, token.Position.Column, parseMessage,
                    token.Symbol.Name, string.Join(",", processor.GetExpectedTokens().Select((symbol) => "'" + symbol.Name + "'"))));
            }
        }
        
        public INode Build(string source)
        {
            return Parse(source);
        }

        private class NodeVisitor
        {
            private readonly INodeBuilder nodeBuilder;

            public static void VisitNode(dynamic node, INodeBuilder nodeBuilder)
            {
                var visitor = new NodeVisitor(nodeBuilder);
                visitor.Visit(node);
            }
            
            NodeVisitor(INodeBuilder nodeBuilder)
            {
                this.nodeBuilder = nodeBuilder;
            }

            void Visit(Statements statements)
            {
                nodeBuilder.StartStatements();

                foreach (var statement in statements)
                    Visit((dynamic) statement);

                nodeBuilder.BuildStatements();
            }

            void Visit(AssignStatement statement)
            {
                Visit((dynamic) statement.Variable);
                Visit((dynamic) statement.Expression);

                nodeBuilder.BuildAssignStatement();
            }

            void Visit(DisplayStatement statement)
            {
                Visit((dynamic) statement.Expression);

                nodeBuilder.BuildDisplayStatement();
            }

            void Visit(DisplayReadStatement statement)
            {
                Visit((dynamic) statement.Variable);
                Visit((dynamic) statement.Expression);

                nodeBuilder.BuildDisplayReadStatement();
            }

            void Visit(IfThenStatement statement)
            {
                Visit((dynamic) statement.Condition);
                Visit((dynamic) statement.ThenBody);

                nodeBuilder.BuildIfThenStatement();
            }

            void Visit(IfThenElseStatement statement)
            {
                Visit((dynamic) statement.Condition);
                Visit((dynamic) statement.ThenBody);
                Visit((dynamic) statement.ElseBody);

                nodeBuilder.BuildIfThenElseStatement();
            }

            void Visit(WhileStatement statement)
            {
                Visit((dynamic) statement.Condition);
                Visit((dynamic) statement.Body);

                nodeBuilder.BuildWhileStatement();
            }

            void Visit(BinaryExpression expression)
            {
                Visit((dynamic) expression.Left);
                Visit((dynamic) expression.Right);
                Visit((dynamic) expression.Op);

                nodeBuilder.BuildBinaryExpression();
            }

            void Visit(ExpressionValue expression)
            {
                Visit((dynamic) expression.Expression);

                nodeBuilder.BuildExpressionValue();
            }

            void Visit(NegateValue expression)
            {
                Visit((dynamic) expression.Value);

                nodeBuilder.BuildNegateValue();
            }

            void Visit(Variable expression)
            {
                nodeBuilder.BuildVariable(expression.Name);
            }

            void Visit(NumberConst expression)
            {
                nodeBuilder.BuildNumberConst(expression.Value);
            }

            void Visit(StringConst expression)
            {
                nodeBuilder.BuildStringConst(expression.Value);
            }

            void Visit(AddOperator op)
            {
                nodeBuilder.BuildAddOp();
            }

            void Visit(SubtractOperator op)
            {
                nodeBuilder.BuildSubOp();
            }

            void Visit(MultiplyOperator op)
            {
                nodeBuilder.BuildMulOp();
            }

            void Visit(DivideOperator op)
            {
                nodeBuilder.BuildDivOp();
            }

            void Visit(LessThanOperator op)
            {
                nodeBuilder.BuildLTOp();
            }

            void Visit(LessThanOrEqualOperator op)
            {
                nodeBuilder.BuildLTOEqOp();
            }

            void Visit(EqualOperator op)
            {
                nodeBuilder.BuildEqOp();
            }

            void Visit(NotEqualOperator op)
            {
                nodeBuilder.BuildNEqOp();
            }

            void Visit(GreaterThanOperator op)
            {
                nodeBuilder.BuildGTOp();
            }

            void Visit(GreaterThanOrEqualOperator op)
            {
                nodeBuilder.BuildGTOEqOp();
            }

        }

        public void BuildNodes(string source, INodeBuilder builder)
        {
            var nodes = Parse(source);

            NodeVisitor.VisitNode(nodes, builder);
            Marshal.ReleaseComObject(builder);
        }
    }
}
