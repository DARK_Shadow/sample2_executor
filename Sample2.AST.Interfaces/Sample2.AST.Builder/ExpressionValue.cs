using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public class ExpressionValue : Value,  IExpressionValue
    {
        private readonly Expression expression;

        [Rule("<Value> ::= ~'(' <Expression> ~')'")]
        public ExpressionValue(Expression expression)
        {
            this.expression = expression;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_ExprVal(this);
        }

        public IExpression Expression
        {
            get { return expression; }
        }
    }
}