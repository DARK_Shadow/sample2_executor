using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    [Terminal("-")]
    public class SubtractOperator : Operator, ISubOperator
    {
        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_SubOp(this);
        }
    }
}