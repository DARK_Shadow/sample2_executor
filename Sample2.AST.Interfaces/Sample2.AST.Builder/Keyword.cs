using bsn.GoldParser.Semantic;

namespace Sample2.AST.Builder
{
    [Terminal("display")]
    [Terminal("read")]
    [Terminal("assign")]
    [Terminal("while")]
    [Terminal("do")]
    [Terminal("end")]
    [Terminal("if")]
    [Terminal("then")]
    [Terminal("else")]
    public class Keyword : Node { }
}