using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    class DisplayReadStatement : DisplayStatement, IDisplayReadStatement
    {
        private readonly Variable variable;

        [Rule(@"<Statement> ::= ~display <Expression> ~read Id")]
        public DisplayReadStatement(Expression expression, Variable variable)
            : base(expression)
        {
            this.variable = variable;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_DsplRdStmt(this);
        }

        public IVariable Variable
        {
            get { return variable; }
        }
    }
}