using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public class DisplayStatement : Statement, IDisplayStatement
    {
        private readonly Expression expression;

        [Rule(@"<Statement> ::= ~display <Expression>")]
        public DisplayStatement(Expression expression)
        {
            this.expression = expression;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_DsplStmt(this);
        }

        public IExpression Expression
        {
            get { return expression; }
        }
    }
}