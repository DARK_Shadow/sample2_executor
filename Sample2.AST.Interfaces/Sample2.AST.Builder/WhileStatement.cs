using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public class WhileStatement : Statement, IWhileStatement
    {
        private readonly Expression condition;
        private readonly Statements body;

        [Rule(@"<Statement> ::= ~while <Expression> ~do <Statements> ~end")]
        public WhileStatement(Expression condition, Statements body)
        {
            this.condition = condition;
            this.body = body;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_WhStmt(this);
        }

        public IExpression Condition
        {
            get { return condition; }
        }

        public IStatements Body
        {
            get { return body; }
        }
    }
}