using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public class BinaryExpression : Expression, IBinaryExpression
    {
        private readonly Expression left;
        private readonly Expression right;
        private readonly Operator op;

        [Rule(@"<Expression> ::= <Expression> '>' <Add Exp>")]
        [Rule(@"<Expression> ::= <Expression> '<' <Add Exp>")]
        [Rule(@"<Expression> ::= <Expression> '<=' <Add Exp>")]
        [Rule(@"<Expression> ::= <Expression> '>=' <Add Exp>")]
        [Rule(@"<Expression> ::= <Expression> '==' <Add Exp>")]
        [Rule(@"<Expression> ::= <Expression> '<>' <Add Exp>")]
        [Rule(@"<Add Exp> ::= <Add Exp> '+' <Mult Exp>")]
        [Rule(@"<Add Exp> ::= <Add Exp> '-' <Mult Exp>")]
        [Rule(@"<Mult Exp> ::= <Mult Exp> '*' <Negate Exp>")]
        [Rule(@"<Mult Exp> ::= <Mult Exp> '/' <Negate Exp>")]
        public BinaryExpression(Expression left, Operator op, Expression right)
        {
            this.left = left;
            this.right = right;
            this.op = op;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_BinExpr(this);
        }

        public IExpression Left
        {
            get { return left; }
        }

        public IExpression Right
        {
            get { return right; }
        }

        public IOperator Op
        {
            get { return op; }
        }
    }
}