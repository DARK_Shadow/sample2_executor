﻿using System;
using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    [Terminal("(EOF)")]
    [Terminal("(Error)")]
    [Terminal("(Whitespace)")]
    [Terminal("&")]
    [Terminal("(")]
    [Terminal(")")]
    [Terminal("=")]
    public class Node : SemanticToken, INode
    {
        public virtual void Accept(IVisitor visitor)
        {
            throw new Exception("'INode.Accept' is not implemented on type: '" + this.GetType().ToString() + "'");
        }
    }
}
