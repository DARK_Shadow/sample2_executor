using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public class IfThenElseStatement : IfThenStatement, IIfThenElseStatement
    {
        private readonly Statements elseStatements; 

        [Rule(@"<Statement> ::= ~if <Expression> ~then <Statements> ~else <Statements> ~end")]
        public IfThenElseStatement(Expression condition, Statements thenStatements, Statements elseStatements)
            : base(condition, thenStatements)
        {
            this.elseStatements = elseStatements;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_IfThElStmt(this);
        }

        public IStatements ElseBody
        {
            get { return elseStatements; } 
        }
    }
}