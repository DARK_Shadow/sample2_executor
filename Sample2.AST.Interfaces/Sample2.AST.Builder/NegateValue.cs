using bsn.GoldParser.Semantic;
using Sample2.AST.Interfaces;

namespace Sample2.AST.Builder
{
    public class NegateValue : Value, INegateValue
    {
        private readonly Value value;

        [Rule(@"<Negate Exp> ::= ~'-' <Value>")]
        public NegateValue(Value value)
        {
            this.value = value;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit_NegVal(this);
        }

        public IValue Value
        {
            get { return value; }
        }
    }
}