unit uNodes;

interface

uses
  Sample2_AST_Interfaces_TLB;

type
  TNode = class abstract(TInterfacedObject, INode)
  protected
    procedure Accept(const Visitor: IVisitor); virtual; safecall; abstract;
  end;

  TOperator = class abstract(TNode, IOperator)
  end;

  TExpression = class abstract(TNode, IExpression)
  end;

  TStatement = class abstract(TNode, IStatement)
  end;

  TCardinalArrayHelper = record helper for TArray<Cardinal>
  public
    function Sum: UInt64;
    function Diviation(const APoint: Cardinal): TArray<Cardinal>;
  end;

implementation

{ TCardinalArrayHelper }

function TCardinalArrayHelper.Sum: UInt64;
var
  LValue: Cardinal;
begin
  Result := 0;
  for LValue in Self do
    Inc(Result, LValue);
end;

function TCardinalArrayHelper.Diviation(const APoint: Cardinal): TArray<Cardinal>;
var
  LValue: Cardinal;
  LIndex: Cardinal;
begin
  SetLength(Result, Length(Self));
  LIndex := 0;
  for LValue in Self do
  begin
    Result[LIndex] := Abs(LValue - APoint);
    Inc(LIndex);
  end;
end;

end.
