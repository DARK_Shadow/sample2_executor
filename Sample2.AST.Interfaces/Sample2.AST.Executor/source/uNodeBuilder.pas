unit uNodeBuilder;

interface

uses
  System.SysUtils,
  Sample2_AST_Interfaces_TLB,
  Sample2_AST_Builder_TLB;

function Build(const ABuilder: IBuilder; const ASource: String): INode;

implementation

uses
  System.Rtti,
  System.Generics.Collections,
  System.Generics.Defaults,
  System.TypInfo,
  uOperators,
  uExpressions,
  uStatements;

type
  TNodeBuilder = class({TSingletonImplementation} TInterfacedObject, INodeBuilder)
  private
    FStack: TStack<TValue>;
  private
    procedure Push<T>(const ANode: T);
    function Pop<T>: T;
    function Peek<T>: T;
  protected
    { INodeBuilder }
    procedure StartStatements; safecall;
    procedure BuildStatements; safecall;
    procedure BuildAssignStatement; safecall;
    procedure BuildDisplayStatement; safecall;
    procedure BuildDisplayReadStatement; safecall;
    procedure BuildIfThenStatement; safecall;
    procedure BuildIfThenElseStatement; safecall;
    procedure BuildWhileStatement; safecall;
    procedure BuildBinaryExpression; safecall;
    procedure BuildExpressionValue; safecall;
    procedure BuildNegateValue; safecall;
    procedure BuildVariable(const name: WideString); safecall;
    procedure BuildNumberConst(value: Double); safecall;
    procedure BuildStringConst(const value: WideString); safecall;
    procedure BuildAddOp; safecall;
    procedure BuildSubOp; safecall;
    procedure BuildMulOp; safecall;
    procedure BuildDivOp; safecall;
    procedure BuildEqOp; safecall;
    procedure BuildNEqOp; safecall;
    procedure BuildLTOp; safecall;
    procedure BuildLTOEqOp; safecall;
    procedure BuildGTOp; safecall;
    procedure BuildGTOEqOp; safecall;
  public
    constructor Create;
    destructor Destroy; override;
    { TFunc<IBuilder,INode> }
    function Invoke(ABuilder: IBuilder; const ASource: String): INode;
  end;

function Build(const ABuilder: IBuilder; const ASource: String): INode;
var
  LBuilder: IInterface;
begin
  LBuilder := TNodeBuilder.Create;
  Result := TNodeBuilder(LBuilder).Invoke(ABuilder, ASource);
//  with TNodeBuilder.Create do
//  try
//    Result := Invoke(ABuilder, ASource);
//  finally
//    Free;
//  end;
end;

{ TNodeBuilder }

procedure TNodeBuilder.BuildAddOp;
begin
  Push<IOperator>(TAddOp.Create);
end;

procedure TNodeBuilder.BuildAssignStatement;
var
  LExpr: IExpression;
  LVar: IVariable;
begin
  LExpr := Pop<IExpression>;
  LVar := Pop<IExpression> as IVariable;

  Peek<TList<IStatement>>.Add(TAssignStatement.Create(LVar, LExpr))
end;

procedure TNodeBuilder.BuildBinaryExpression;
var
  LLeft: IExpression;
  LRight: IExpression;
  LOp: IOperator;
begin
  LOp := Pop<IOperator>;
  LRight := Pop<IExpression>;
  LLeft := Pop<IExpression>;

  Push<IExpression>(TBinaryExpression.Create(LLeft, LRight, LOp));
end;

procedure TNodeBuilder.BuildDisplayReadStatement;
var
  LExpr: IExpression;
  LVar: IVariable;
begin
  LExpr := Pop<IExpression>;
  LVar := Pop<IExpression> as IVariable;

  Peek<TList<IStatement>>.Add(TDisplayReadStatement.Create(LExpr, LVar))
end;

procedure TNodeBuilder.BuildDisplayStatement;
var
  LExpr: IExpression;
begin
  LExpr := Pop<IExpression>;

  Peek<TList<IStatement>>.Add(TDisplayStatement.Create(LExpr))
end;

procedure TNodeBuilder.BuildDivOp;
begin
  Push<IOperator>(TDivOp.Create);
end;

procedure TNodeBuilder.BuildEqOp;
begin
  Push<IOperator>(TEqOp.Create);
end;

procedure TNodeBuilder.BuildExpressionValue;
var
  LExpr: IExpression;
begin
  LExpr := Pop<IExpression>;

  Push<IExpression>(TExpressionValue.Create(LExpr));
end;

procedure TNodeBuilder.BuildGTOEqOp;
begin
  Push<IOperator>(TGTOEqOp.Create);
end;

procedure TNodeBuilder.BuildGTOp;
begin
  Push<IOperator>(TGTOp.Create);
end;

procedure TNodeBuilder.BuildIfThenElseStatement;
var
  LCond: IExpression;
  LThen: IStatements;
  LElse: IStatements;
begin
  LElse := Pop<IStatements>;
  LThen := Pop<IStatements>;
  LCond := Pop<IExpression>;

  Peek<TList<IStatement>>.Add(TIfThenElseStatement.Create(LCond, LThen, LElse));
end;

procedure TNodeBuilder.BuildIfThenStatement;
var
  LCond: IExpression;
  LThen: IStatements;
begin
  LThen := Pop<IStatements>;
  LCond := Pop<IExpression>;

  Peek<TList<IStatement>>.Add(TIfThenStatement.Create(LCond, LThen));
end;

procedure TNodeBuilder.BuildLTOEqOp;
begin
  Push<IOperator>(TLTOEqOp.Create);
end;

procedure TNodeBuilder.BuildLTOp;
begin
  Push<IOperator>(TLTOp.Create);
end;

procedure TNodeBuilder.BuildMulOp;
begin
  Push<IOperator>(TMulOp.Create);
end;

procedure TNodeBuilder.BuildNegateValue;
var
  LValue: IValue;
begin
  LValue := Pop<IExpression> as IValue;

  Push<IExpression>(TNegateValue.Create(LValue));
end;

procedure TNodeBuilder.BuildNEqOp;
begin
  Push<IOperator>(TNEqOp.Create);
end;

procedure TNodeBuilder.BuildNumberConst(value: Double);
begin
  Push<IExpression>(TNumberConst.Create(value));
end;

procedure TNodeBuilder.BuildStatements;
var
  LStatements: TList<IStatement>;
  LStatement: IStatement;
begin
  LStatements := Pop<TList<IStatement>>;

  Push<IStatements>(TStatements.Create(LStatements));
end;

procedure TNodeBuilder.BuildStringConst(const value: WideString);
begin
  Push<IExpression>(TStringConst.Create(value));
end;

procedure TNodeBuilder.BuildSubOp;
begin
  Push<IOperator>(TSubOp.Create);
end;

procedure TNodeBuilder.BuildVariable(const name: WideString);
begin
  Push<IExpression>(TVariable.Create(name));
end;

procedure TNodeBuilder.BuildWhileStatement;
var
  LBody: IStatements;
  LCond: IExpression;
begin
  LBody := Pop<IStatements>;
  LCond := Pop<IExpression>;

  Peek<TList<IStatement>>.Add(TWhileStatement.Create(LCond, LBody));
end;

constructor TNodeBuilder.Create;
begin
  inherited;
  FStack := TStack<TValue>.Create;
end;

destructor TNodeBuilder.Destroy;
begin
  FStack.Free;
  inherited;
end;

function TNodeBuilder.Invoke(ABuilder: IBuilder; const ASource: String): INode;
begin
  FStack.Clear;
  ABuilder.BuildNodes(ASource, Self);
  Result := FStack.Pop.AsType<IStatements>;
end;

function TNodeBuilder.Peek<T>: T;
var
  LValue: TValue;
begin
  LValue := FStack.Peek;
  try
    Result := LValue.AsType<T>;
  except
    Exception.RaiseOuterException(EInvalidCast.CreateFmt('Expected: %s; Actual: %s;',
      [GetTypeName(TypeInfo(T)), GetTypeName(LValue.TypeInfo)]));
  end;
end;

function TNodeBuilder.Pop<T>: T;
var
  LValue: TValue;
begin
  LValue := FStack.Pop;
  try
    Result := LValue.AsType<T>;
  except
    Exception.RaiseOuterException(EInvalidCast.CreateFmt('Expected: %s; Actual: %s;',
      [GetTypeName(TypeInfo(T)), GetTypeName(LValue.TypeInfo)]));
  end;
end;

procedure TNodeBuilder.Push<T>(const ANode: T);
begin
  FStack.Push(TValue.From(ANode));
end;

procedure TNodeBuilder.StartStatements;
begin
  Push<TList<IStatement>>(TList<IStatement>.Create);
end;

end.
