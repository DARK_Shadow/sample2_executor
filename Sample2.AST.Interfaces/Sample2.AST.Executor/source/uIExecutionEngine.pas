unit uIExecutionEngine;

interface

uses
  uIExecutionContext, Sample2_AST_Interfaces_TLB;

type
  IExecutionEngine = interface
    procedure Execute(const ANode: INode; const AContext: IExecutionContext);
  end;

implementation

end.
