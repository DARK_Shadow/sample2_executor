unit uExecutionContext;

interface

uses
  uIExecutionContext;

function CreateContext: IExecutionContext;

implementation

uses
  System.Rtti,
  System.Generics.Collections;

type
  TExecutionContext = class(TInterfacedObject, IExecutionContext)
  private
    FValues: TDictionary<String,TValue>;
  protected
    function GetValue(const AName: string): TValue;
    procedure SetValue(const AName: string; const AValue: TValue);
  public
    constructor Create;
    destructor Destroy; override;
  end;

function CreateContext: IExecutionContext;
begin
  Result := TExecutionContext.Create;
end;

{ TExecutionContext }

constructor TExecutionContext.Create;
begin
  inherited;
  FValues := TDictionary<String,TValue>.Create;
end;

destructor TExecutionContext.Destroy;
begin
  FValues.Free;
  inherited;
end;

function TExecutionContext.GetValue(const AName: string): TValue;
begin
  Result := FValues[AName];
end;

procedure TExecutionContext.SetValue(const AName: string; const AValue: TValue);
begin
  FValues.AddOrSetValue(AName, AValue);
end;

end.
