unit uIExecutionContext;

interface

uses
  System.Rtti;

type
  IExecutionContext = interface
    function GetValue(const AName: String): TValue;
    procedure SetValue(const AName: String; const AValue: TValue);

    property Values[const AName: String]: TValue read GetValue write SetValue; default;
  end;


implementation

end.
