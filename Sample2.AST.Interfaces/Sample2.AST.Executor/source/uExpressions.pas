unit uExpressions;

interface

uses
  Sample2_AST_Interfaces_TLB,
  uNodes;

type
  TBinaryExpression = class(TExpression, IBinaryExpression)
  private
    FLeft: IExpression;
    FRight: IExpression;
    FOp: IOperator;
  protected
    { IBinaryExpression }
    function get_Left: IExpression; safecall;
    function get_Op: IOperator; safecall;
    function get_Right: IExpression; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  public
    constructor Create(const ALeft, ARight: IExpression; const AOp: IOperator);
  end;

  TValue<T> = class abstract(TExpression, IValue)
  strict private
    FValue: T;
  protected
    property Value: T read FValue;
  public
    constructor Create(const AValue: T);
  end;

  TExpressionValue = class(TValue<IExpression>, IExpressionValue)
  protected
    { IExpressionValue }
    function get_Expression: IExpression; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TNegateValue = class(TValue<IValue>, INegateValue)
  protected
    { INegateValue }
    function get_Value: IValue; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TVariable = class(TValue<String>, IVariable)
  protected
    { IVariable }
    function get_Name: WideString; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TConst<T> = class abstract(TValue<T>, IConst)
  end;

  TNumberConst = class(TConst<Double>, INumberConst)
  protected
    { INumberConst }
    function get_Value: Double; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TStringConst = class(TConst<String>, IStringConst)
  protected
    { IStringConst }
    function get_Value: WideString; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

implementation

{ TBinaryExpression }

procedure TBinaryExpression.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_BinExpr(Self);
end;

constructor TBinaryExpression.Create(const ALeft, ARight: IExpression;
  const AOp: IOperator);
begin
  inherited Create;
  FLeft := ALeft;
  FRight := ARight;
  FOp := AOp;
end;

function TBinaryExpression.get_Left: IExpression;
begin
  Result := FLeft;
end;

function TBinaryExpression.get_Op: IOperator;
begin
  Result := FOp;
end;

function TBinaryExpression.get_Right: IExpression;
begin
  Result := FRight;
end;

{ TExpressionValue }

procedure TExpressionValue.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_ExprVal(Self);
end;

function TExpressionValue.get_Expression: IExpression;
begin
  Result := Value;
end;

{ TNegateValue }

procedure TNegateValue.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_NegVal(Self);
end;

function TNegateValue.get_Value: IValue;
begin
  Result := Value;
end;

{ TVariable }

procedure TVariable.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_Var(Self);
end;

function TVariable.get_Name: WideString;
begin
  Result := Value;
end;

{ TValue<T> }

constructor TValue<T>.Create(const AValue: T);
begin
  inherited Create;
  FValue := AValue;
end;

{ TNumberConst }

procedure TNumberConst.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_NumCnst(Self);
end;

function TNumberConst.get_Value: Double;
begin
  Result := Value;
end;

{ TStringConst }

procedure TStringConst.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_StrCnst(Self);
end;

function TStringConst.get_Value: WideString;
begin
  Result := Value;
end;

end.
