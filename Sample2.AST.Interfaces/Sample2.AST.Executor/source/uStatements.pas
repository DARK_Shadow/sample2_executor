unit uStatements;

interface

uses
  System.Generics.Collections,
  Sample2_AST_Interfaces_TLB,
  uNodes;

type
  TAssignStatement = class(TStatement, IAssignStatement)
  private
    FVariable: IVariable;
    FExpression: IExpression;
  protected
    { IAssignStatement }
    function get_Expression: IExpression; safecall;
    function get_Variable: IVariable; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  public
    constructor Create(const AVariable: IVariable; const AExpression: IExpression);
  end;

  TDisplayStatement = class(TStatement, IDisplayStatement)
  private
    FExpression: IExpression;
  protected
    { IDisplayStatement }
    function get_Expression: IExpression; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  public
    constructor Create(const AExpression: IExpression);
  end;

  TDisplayReadStatement = class(TDisplayStatement, IDisplayReadStatement)
  private
    FVariable: IVariable;
  protected
    { IDisplayReadStatement }
    function get_Variable: IVariable; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  public
    constructor Create(const AExpression: IExpression; const AVariable: IVariable);
  end;

  TIfThenStatement = class(TStatement, IIfThenStatement)
  private
    FCondition: IExpression;
    FThenBody: IStatements;
  protected
    { IIfThenStatement }
    function get_Condition: IExpression; safecall;
    function get_ThenBody: IStatements; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  public
    constructor Create(const ACondition: IExpression; const AThenBody: IStatements);
  end;

  TIfThenElseStatement = class(TIfThenStatement, IIfThenElseStatement)
  private
    FElseBody: IStatements;
  protected
    { IIfThenElseStatement }
    function get_ElseBody: IStatements; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  public
    constructor Create(const ACondition: IExpression; const AThenBody, AElseBody: IStatements);
  end;

  TWhileStatement = class(TStatement, IWhileStatement)
  private
    FCondition: IExpression;
    FBody: IStatements;
  protected
    { IWhileStatement }
    function get_Condition: IExpression; safecall;
    function get_Body: IStatements; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  public
    constructor Create(const ACondition: IExpression; const ABody: IStatements);
  end;

  TStatements = class(TStatement, IStatements)
  private type
    TStatementsEnumerator = class(TInterfacedObject, IStatementsEnumerator)
    private
      FEnumerator: TEnumerator<IStatement>;
    protected
      { IStatementsEnumerator }
      function MoveNext: WordBool; safecall;
      function get_Current: IStatement; safecall;
    public
      constructor Create(const AEnumerator: TEnumerator<IStatement>);
      destructor Destroy; override;
    end;

  private
    FStatements: TEnumerable<IStatement>;
    FOwnEnum: Boolean;
  protected
    { IStatements }
    function GetEnumerator: IStatementsEnumerator; safecall;
    { TNode }
    procedure Accept(const Visitor: IVisitor); override; safecall;
  public
    constructor Create(const AStatements: TEnumerable<IStatement>; const AOwnEnum: Boolean = True);
    destructor Destroy; override;
  end;

implementation

{ TAssignStatement }

procedure TAssignStatement.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_AsgnStmt(Self);
end;

constructor TAssignStatement.Create(const AVariable: IVariable;
  const AExpression: IExpression);
begin
  inherited Create;
  FVariable := AVariable;
  FExpression := AExpression;
end;

function TAssignStatement.get_Expression: IExpression;
begin
  Result := FExpression;
end;

function TAssignStatement.get_Variable: IVariable;
begin
  Result := FVariable;
end;

{ TDisplayStatement }

procedure TDisplayStatement.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_DsplStmt(Self);
end;

constructor TDisplayStatement.Create(const AExpression: IExpression);
begin
  inherited Create;
  FExpression := AExpression;
end;

function TDisplayStatement.get_Expression: IExpression;
begin
  Result := FExpression;
end;

{ TDisplayReadStatement }

procedure TDisplayReadStatement.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_DsplRdStmt(Self);
end;

constructor TDisplayReadStatement.Create(const AExpression: IExpression;
  const AVariable: IVariable);
begin
  inherited Create(AExpression);
  FVariable := AVariable;
end;

function TDisplayReadStatement.get_Variable: IVariable;
begin
  Result := FVariable;
end;

{ TIfThenStatement }

procedure TIfThenStatement.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_IfThStmt(Self);
end;

constructor TIfThenStatement.Create(const ACondition: IExpression;
  const AThenBody: IStatements);
begin
  inherited Create;
  FCondition := ACondition;
  FThenBody := AThenBody;
end;

function TIfThenStatement.get_Condition: IExpression;
begin
  Result := FCondition;
end;

function TIfThenStatement.get_ThenBody: IStatements;
begin
  Result := FThenBody;
end;

{ TIfThenElseStatement }

procedure TIfThenElseStatement.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_IfThElStmt(Self);
end;

constructor TIfThenElseStatement.Create(const ACondition: IExpression;
  const AThenBody, AElseBody: IStatements);
begin
  inherited Create(ACondition, AThenBody);
  FElseBody := AElseBody;
end;

function TIfThenElseStatement.get_ElseBody: IStatements;
begin
  Result := FElseBody;
end;

{ TWhileStatement }

procedure TWhileStatement.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_WhStmt(Self);
end;

constructor TWhileStatement.Create(const ACondition: IExpression;
  const ABody: IStatements);
begin
  inherited Create;
  FCondition := ACondition;
  FBody := ABody;
end;

function TWhileStatement.get_Body: IStatements;
begin
  Result := FBody;
end;

function TWhileStatement.get_Condition: IExpression;
begin
  Result := FCondition;
end;

{ TStatements.TStatementsEnumerator }

constructor TStatements.TStatementsEnumerator.Create(
  const AEnumerator: TEnumerator<IStatement>);
begin
  inherited Create;
  FEnumerator := AEnumerator;
end;

destructor TStatements.TStatementsEnumerator.Destroy;
begin
  if Assigned(FEnumerator) then
    FEnumerator.Free;
  inherited;
end;

function TStatements.TStatementsEnumerator.get_Current: IStatement;
begin
  Result := FEnumerator.Current;
end;

function TStatements.TStatementsEnumerator.MoveNext: WordBool;
begin
  Result := FEnumerator.MoveNext;
end;

{ TStatements }

procedure TStatements.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_Stmts(Self);
end;

constructor TStatements.Create(const AStatements: TEnumerable<IStatement>; const AOwnEnum: Boolean);
begin
  inherited Create;
  FStatements := AStatements;
  FOwnEnum := AOwnEnum;
end;

destructor TStatements.Destroy;
begin
  if FOwnEnum then
    FStatements.Free;
  inherited;
end;

function TStatements.GetEnumerator: IStatementsEnumerator;
begin
  Result := TStatementsEnumerator.Create(FStatements.GetEnumerator);
end;

end.
