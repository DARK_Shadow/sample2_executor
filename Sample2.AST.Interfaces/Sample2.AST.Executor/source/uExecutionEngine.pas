unit uExecutionEngine;

interface

uses
  uIExecutionEngine;

function CreateIntepretator: IExecutionEngine;

implementation

uses
  System.SysUtils,
  Sample2_AST_Interfaces_TLB,
  uIExecutionContext,
  System.Rtti,
  System.Generics.Collections,
  System.Generics.Defaults,
  Winapi.ActiveX,
  System.Win.ComObj;

type
  TExecutor = class(TSingletonImplementation, IVisitor, ISupportErrorInfo)
  strict private
    FContext: IExecutionContext;
    FStack: TStack<TValue>;
  protected
    { IVisitor }
    procedure Visit_Var(const Node: IVariable); safecall;
    procedure Visit_DsplRdStmt(const Node: IDisplayReadStatement); safecall;
    procedure Visit_AsgnStmt(const Node: IAssignStatement); safecall;
    procedure Visit_AddOp(const Node: IAddOperator); safecall;
    procedure Visit_SubOp(const Node: ISubOperator); safecall;
    procedure Visit_MulOp(const Node: IMulOperator); safecall;
    procedure Visit_DivOp(const Node: IDivOperator); safecall;
    procedure Visit_LTOp(const Node: ILTOperator); safecall;
    procedure Visit_LTEqOp(const Node: ILTEqOperator); safecall;
    procedure Visit_EqOp(const Node: IEqOperator); safecall;
    procedure Visit_NEqOp(const Node: INEqOperator); safecall;
    procedure Visit_GTOp(const Node: IGTOperator); safecall;
    procedure Visit_GTEqOp(const Node: IGTEqOperator); safecall;
    procedure Visit_NumCnst(const Node: INumberConst); safecall;
    procedure Visit_StrCnst(const Node: IStringConst); safecall;
    procedure Visit_NegVal(const Node: INegateValue); safecall;
    procedure Visit_DsplStmt(const Node: IDisplayStatement); safecall;
    procedure Visit_WhStmt(const Node: IWhileStatement); safecall;
    procedure Visit_IfThStmt(const Node: IIfThenStatement); safecall;
    procedure Visit_IfThElStmt(const Node: IIfThenElseStatement); safecall;
    procedure Visit_ExprVal(const Node: IExpressionValue); safecall;
    procedure Visit_BinExpr(const Node: IBinaryExpression); safecall;
    procedure Visit_Stmts(const Node: IStatements); safecall;
    { ISupportErrorInfo }
    function InterfaceSupportsErrorInfo(const iid: TGUID): HRESULT; stdcall;
  public
    constructor Create(const AContext: IExecutionContext);
    { TObject }
    destructor Destroy; override;
    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer): HRESULT; override;
  end;

  TExecutionEngine = class(TInterfacedObject, IExecutionEngine)
  protected
    { IExecutionEngine }
    procedure Execute(const ANode: INode; const AContext: IExecutionContext);
  end;

function CreateIntepretator: IExecutionEngine;
begin
  Result := TExecutionEngine.Create;
end;

{ TVisitor }

procedure TExecutionEngine.Execute(const ANode: INode; const AContext: IExecutionContext);
var
  LVisitor: TExecutor;
begin
  LVisitor := TExecutor.Create(AContext);
  try
    ANode.Accept(LVisitor);
  finally
    LVisitor.Free;;
  end;
end;

procedure TExecutor.Visit_Var(const Node: IVariable);
begin
  FStack.Push(FContext[Node.Name]);
end;

procedure TExecutor.Visit_DsplRdStmt(const Node: IDisplayReadStatement);
var
  LString: String;
  LNumber: Double;
  LValue: TValue;
  LExpression: String;
begin
  Node.Expression.Accept(Self);
  LExpression := FStack.Pop.ToString;
//  Write(LExpression);
  Readln(LString);
  if Double.TryParse(LString, LNumber) then
    LValue := LNumber
  else
    LValue := LString;
  FContext[Node.Variable.Name] := LValue;
end;

procedure TExecutor.Visit_AsgnStmt(const Node: IAssignStatement);
begin
  Node.Expression.Accept(Self);
  FContext[Node.Variable.Name] := FStack.Pop;
end;

procedure TExecutor.Visit_AddOp(const Node: IAddOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended + LRight.AsExtended);
end;

procedure TExecutor.Visit_SubOp(const Node: ISubOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended - LRight.AsExtended);
end;

procedure TExecutor.Visit_MulOp(const Node: IMulOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended * LRight.AsExtended);
end;

procedure TExecutor.Visit_DivOp(const Node: IDivOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended / LRight.AsExtended);
end;

procedure TExecutor.Visit_LTOp(const Node: ILTOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended < LRight.AsExtended);
end;

procedure TExecutor.Visit_LTEqOp(const Node: ILTEqOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended <= LRight.AsExtended);
end;

procedure TExecutor.Visit_EqOp(const Node: IEqOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended = LRight.AsExtended);
end;

procedure TExecutor.Visit_NEqOp(const Node: INEqOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended <> LRight.AsExtended);
end;

procedure TExecutor.Visit_GTOp(const Node: IGTOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended > LRight.AsExtended);
end;

procedure TExecutor.Visit_GTEqOp(const Node: IGTEqOperator);
var
  LLeft: TValue;
  LRight: TValue;
begin
  LLeft := FStack.Pop;
  LRight := FStack.Pop;
  FStack.Push(LLeft.AsExtended >= LRight.AsExtended);
end;

procedure TExecutor.Visit_NumCnst(const Node: INumberConst);
begin
  FStack.Push(Node.Value);
end;

procedure TExecutor.Visit_StrCnst(const Node: IStringConst);
begin
  FStack.Push(Node.Value);
end;

procedure TExecutor.Visit_NegVal(const Node: INegateValue);
begin
  Node.Value.Accept(Self);
  FStack.Push(-FStack.Pop.AsExtended);
end;

procedure TExecutor.Visit_DsplStmt(const Node: IDisplayStatement);
var
  LValue: String;
begin
  Node.Expression.Accept(Self);
  LValue := FStack.Pop.ToString;
//  Writeln(LValue);
end;

procedure TExecutor.Visit_WhStmt(const Node: IWhileStatement);
begin
  Node.Condition.Accept(Self);
  while FStack.Pop.AsBoolean do
  begin
    Node.Body.Accept(Self);
    Node.Condition.Accept(Self);
  end;
end;

procedure TExecutor.Visit_IfThStmt(const Node: IIfThenStatement);
begin
  Node.Condition.Accept(Self);
  if FStack.Pop.AsBoolean then
    Node.ThenBody.Accept(Self);
end;

procedure TExecutor.Visit_IfThElStmt(const Node: IIfThenElseStatement);
begin
  Node.Condition.Accept(Self);
  if FStack.Pop.AsBoolean then
    Node.ThenBody.Accept(Self)
  else
    Node.ElseBody.Accept(Self);
end;

constructor TExecutor.Create(const AContext: IExecutionContext);
begin
  inherited Create;
  FStack := TStack<TValue>.Create;
  FContext := AContext;
end;

destructor TExecutor.Destroy;
begin
  FStack.Free;
  inherited;
end;

function TExecutor.InterfaceSupportsErrorInfo(const iid: TGUID): HRESULT;
begin
  Result := S_OK;
end;

procedure TExecutor.Visit_ExprVal(const Node: IExpressionValue);
begin
  Node.Expression.Accept(Self);
end;

procedure TExecutor.Visit_BinExpr(const Node: IBinaryExpression);
begin
  Node.Right.Accept(Self);
  Node.Left.Accept(Self);
  Node.Op.Accept(Self);
end;

procedure TExecutor.Visit_Stmts(const Node: IStatements);
var
  LStatement: IStatement;
begin
  for LStatement in Node do
    LStatement.Accept(Self);
end;

function TExecutor.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HRESULT;
begin
  Result := HandleSafeCallException(ExceptObject, ExceptAddr,
    IID_IVisitor, '', '');
end;

end.
