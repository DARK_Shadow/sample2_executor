unit uOperators;

interface

uses
  Sample2_AST_Interfaces_TLB,
  uNodes;

type
  TAddOp = class(TOperator, IAddOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TSubOp = class(TOperator, ISubOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TMulOp = class(TOperator, IMulOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TDivOp = class(TOperator, IDivOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TEqOp = class(TOperator, IEqOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TNEqOp = class(TOperator, INEqOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TLTOp = class(TOperator, ILTOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TLTOEqOp = class(TOperator, ILTEqOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TGTOp = class(TOperator, IGTOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

  TGTOEqOp = class(TOperator, IGTEqOperator)
  protected
    procedure Accept(const Visitor: IVisitor); override; safecall;
  end;

implementation

{ TAddOp }

procedure TAddOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_AddOp(Self);
end;

{ TSubOp }

procedure TSubOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_SubOp(Self);
end;

{ TMulOp }

procedure TMulOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_MulOp(Self);
end;

{ TDivOp }

procedure TDivOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_DivOp(Self);
end;

{ TEqOp }

procedure TEqOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_EqOp(Self);
end;

{ TNEqOp }

procedure TNEqOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_NEqOp(Self);
end;

{ TLTOp }

procedure TLTOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_LTOp(Self);
end;

{ TLTOEqOp }

procedure TLTOEqOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_LTEqOp(Self);
end;

{ TGTOp }

procedure TGTOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_GTOp(Self);
end;

{ TGTOEqOp }

procedure TGTOEqOp.Accept(const Visitor: IVisitor);
begin
  Visitor.Visit_GTEqOp(Self);
end;

end.
