@echo off

set in_asm=Sample2.AST.Builder.dll
set out_tlb=Sample2.AST.Builder.tlb

pushd .\bin\Win32\Debug
C:\Windows\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe %in_asm% /tlb /u
C:\Windows\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe %in_asm% /tlb
popd

pushd .\source
tlibimp.exe -P -Pt+ -XM+ ..\bin\Win32\Debug\%out_tlb%
popd