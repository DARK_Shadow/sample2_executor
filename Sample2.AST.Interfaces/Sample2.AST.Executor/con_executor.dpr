program con_executor;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Sample2_AST_Builder_TLB in 'source\Sample2_AST_Builder_TLB.pas',
  mscorlib_TLB in 'source\mscorlib_TLB.pas',
  uExecutionEngine in 'source\uExecutionEngine.pas',
  Sample2_AST_Interfaces_TLB in 'source\Sample2_AST_Interfaces_TLB.pas',
  uExecutionContext in 'source\uExecutionContext.pas',
  uIExecutionContext in 'source\uIExecutionContext.pas',
  uIExecutionEngine in 'source\uIExecutionEngine.pas',
  uNodeBuilder in 'source\uNodeBuilder.pas',
  uNodes in 'source\uNodes.pas',
  uOperators in 'source\uOperators.pas',
  uExpressions in 'source\uExpressions.pas',
  uStatements in 'source\uStatements.pas',
  System.Diagnostics,
  Winapi.ActiveX;

const
  PROG =
  'assign A = 1' + sLineBreak +
  'assign B = 5000' + sLineBreak +
  'while A < B do' + sLineBreak +
  '  display A' + sLineBreak +
  '  assign A = A + 1' + sLineBreak +
  'end' + sLineBreak +
  'assign number = 13' + sLineBreak +
  'if number > 10 then' + sLineBreak +
  '  display ''number > 10''' + sLineBreak +
  'else' + sLineBreak +
  '  display ''number <= 10''' + sLineBreak +
  '' + sLineBreak +
  'end';

procedure Run;

  procedure Execute(const ANode: INode);
  var
    LExecutor: IExecutionEngine;
    LContext: IExecutionContext;
  begin
    LContext := CreateContext;
    LExecutor := CreateIntepretator;
    try
      LExecutor.Execute(ANode, LContext);
    finally
      LExecutor := nil;
      LContext := nil;
    end;
  end;

  procedure TimeCount(const ANodeFactory: TFunc<IBuilder,INode>; const ARunCount: Cardinal = 10);

    procedure WriteResult(const AStage: String; const AResults: TArray<Cardinal>);
    var
      LSum: Cardinal;
    begin
      LSum := AResults.Sum;
      Writeln(AStage, ': ', (LSum / ARunCount):2:3, ' ms +- ', (AResults.Diviation(LSum div ARunCount).Sum / ARunCount):2:3, ' ms');
    end;

  var
    LNode: INode;
    LBuilder: IBuilder;
    LCounter: TStopwatch;
    LBuildCounter: TArray<Cardinal>;
    LExecureCounter: TArray<Cardinal>;
    LIndex: Integer;
  begin
    SetLength(LBuildCounter, ARunCount);
    SetLength(LExecureCounter, ARunCount);

    LBuilder := CoBuilder.Create;
    LCounter := TStopwatch.Create;
    for LIndex := 0 to ARunCount - 1 do
    begin
      LCounter.Start;
      LNode := ANodeFactory(LBuilder);
      LBuildCounter[LIndex] := LCounter.ElapsedMilliseconds;
      LCounter.Reset;
      LCounter.Start;
      Execute(LNode);
      LExecureCounter[LIndex] := LCounter.ElapsedMilliseconds;
      LCounter.Reset;
    end;
    WriteResult('Build', LBuildCounter);
    WriteResult('Execute', LExecureCounter);
  end;

begin
  Writeln('WarmUp');
  TimeCount(
    function(ABuilder: IBuilder): INode
    begin
      Result := ABuilder.Build(PROG);
    end, 1);

  Writeln;
  Writeln('Simple visitor:');
  TimeCount(
    function(ABuilder: IBuilder): INode
    begin
      Result := ABuilder.Build(PROG);
    end, 100);

  Writeln;
  Writeln('Simple listener:');

  TimeCount(
    function(ABuilder: IBuilder): INode
    begin
      Result := Build(ABuilder, PROG);
    end, 100);
end;

begin
  ReportMemoryLeaksOnShutdown := True;
  try
    Run;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
  Writeln('Press any key...');
  Readln;
end.
